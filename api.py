# -*- coding: utf-8 -*-
import time
import json
import hashlib
import functools

from sqlite3 import connect
from flask import Flask, request, render_template, url_for, redirect, jsonify, session

app = Flask(__name__)
app.secret_key = '\xcd\x9ee~\x924n\xe5\xe0)"8\xc0\x0b\xdf4\xaba\xf2x\xe0\xf5\x1c\xee'

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def error():
    return {'status': 'error'}

def run_sql(sql):
    db_connect = connect('sql/database.sqlite3', isolation_level=None)
    db_connect.row_factory = dict_factory
    db_cursor = db_connect.cursor ()
    db_cursor.execute(sql)
    res = db_cursor.fetchall()
    db_connect.close()
    return res

def sign_in(username, password):
    username = unicode(username)
    password = unicode(password)

    res = run_sql('SELECT count(*) as count FROM `users` '
                  'WHERE name="%s" AND password="%s"' %
                  (username, hashlib.md5(password).hexdigest()))

    if res[0]['count'] != 0:
        session['username'] = username
        return True
    else:
        return False

def from_table(table_name):
    ret = run_sql ('SELECT * FROM `%s`' % (table_name,))
    map (lambda x: x.pop('class'), ret)
    return { table_name: ret}
    
def fetch_all():
    ret = {}
    ret['iternal'] = iternal()
    ret['perepherial'] = perepherial()
    return dict(ret.items() + accessories().items())

def iternal():
    return dict(iternal_core().items() +\
                iternal_modular().items())

def iternal_core():
    return from_table('core')
    
def iternal_modular():
    return from_table('modular')

def perepherial():
    return dict(perepherial_io().items() +\
                perepherial_exchange().items())

def perepherial_io():
    return from_table('input_output')

def perepherial_exchange():
    return from_table('exchange')
    
def accessories():
    return from_table('accessories')

def is_logged():
    return True if 'username' in session else False

def check_login(fun):
    @functools.wraps(fun)
    def wrapper(**args):
        if is_logged():
            return fun(**args)
        else:
            return redirect(url_for('login'))
    return wrapper

def render_json(data):
    return jsonify(**data)
    
def render_xml(data):
    pass

@app.route('/')
@check_login
def index():
    return render_template("index.html", 
                            username=session['username'], 
                            page='Home')

@app.route('/help')
@check_login
def help():
    return render_template("help.html", 
                            username=session['username'], 
                            page='Help')

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        if sign_in(request.form.get('username'), 
                   request.form.get('password')):
            return redirect(url_for('index'))
        else:
            return render_template('login.html', error=True)
    else:
        if is_logged():
            return redirect(url_for('index'))
        else:
            return render_template('login.html', error=False)

@app.route('/logout')
@check_login
def logout():
    session.pop('username')
    return redirect(url_for('login'))

@app.route('/show')
@check_login
def show():
    return render_template("show.html", 
                            username=session['username'], 
                            page='Catalog')

@app.route('/success')
@check_login
def success():
    return render_template("success.html",
                            username=session['username'],
                            previous_page=request.args.get('referer'))

@app.route('/add')
@check_login
def add():
    return render_template("add.html", 
                            username=session['username'], 
                            page='Manage content')

@app.route('/api', methods=['POST'])
@app.route('/<action>/api', methods=['POST'])
@check_login
def api(action='show'):
    if action == 'show':
        time.sleep(2)
        content = request.form.get('content', '')
        
        return render_json ({
            'all': fetch_all,
            'iternal': iternal,
            'iternal_core': iternal_core,
            'iternal_modular': iternal_modular,
            'perepherial': perepherial,
            'perepherial_io': perepherial_io,
            'perepherial_exchange': perepherial_exchange,
            'accessories': accessories,
        }.get(content, error) () )
    elif action == 'add':
        table = request.form.get('type', '')
            
        keys = ''
        vals = '' 
        for k,v in request.form.viewitems():
            if k != 'type':
                keys += "%s, " % (k,) 
                vals += "'%s', " % (' '.join(v),)
        res = run_sql("INSERT INTO `%s` (%s) VALUES (%s)" % (table, keys[:-2], vals[:-2]))
        return redirect(url_for('success', referer=url_for('add')))
    elif action == 'delete':
        table = request.form.get('table', '')
        dev_id = request.form.get('dev_id', '')
        if table and dev_id:
            res = run_sql("DELETE FROM `%s` WHERE dev_id='%s'" % (table, dev_id))
            return render_json({'status': 'success'})
        else:
            return render_json({'status': 'error'})

if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')