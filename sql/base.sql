CREATE TABLE `class` (
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL
);

CREATE TABLE `core` (
	dev_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL,
	power INTEGER,
	brand VARCHAR(255),
	model VARCHAR(255),
	class INTEGER,
	FOREIGN KEY(class) REFERENCES class(id)
);

CREATE TABLE `modular` (
	dev_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL,
	power INTEGER,
	slot VARCHAR(255),
	brand VARCHAR(255),
	model VARCHAR(255),
	class INTEGER,
	FOREIGN KEY(class) REFERENCES class(id)
);

CREATE TABLE `input_output` (
	dev_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL,
	connector VARCHAR(255),
	speed VARCHAR(255),
	brand VARCHAR(255),
	class INTEGER,	
	FOREIGN KEY(class) REFERENCES class(id)		
);

CREATE TABLE `exchange` (
	dev_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL,
	connector VARCHAR(255),
	speed VARCHAR(255),
	brand VARCHAR(255),
	digital INT(1),
	description TEXT,
	class INTEGER,	
	FOREIGN KEY(class) REFERENCES class(id)			
);

CREATE TABLE `accessories` (
	dev_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL,
	description TEXT,
	class INTEGER,	
	FOREIGN KEY(class) REFERENCES class(id)			
);

CREATE TABLE `users` (
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL
);