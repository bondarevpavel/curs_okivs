MyApp = {}
MyApp.back_to_top = {
    offset: 200,
    duration: 500,
}


$(document).ready(function(){
    smooth_show('#content');

    $(".catalog-navigation li").click(function(e){
        e.preventDefault();
        $(this).siblings('.active').removeClass('active');
        $(this).addClass('active');
    
        smooth_show("#spinningSquaresG");
        $(".catalog-table").hide();
        $.ajax({
            url: '/api',
            type: 'POST',
            dataType: 'json',
            data: {
                content: this.children[0].getAttribute('rel'),
            },
        }).done(function(resp){
            $("#content .catalog-table").html("");
            $.each(resp, create_table);
            $("#spinningSquaresG").hide();
            smooth_show(".catalog-table");
        });
    });

    $("#add-selector").change(function(e){
        $("form.add-form").hide();
        $("#add-" + this.selectedOptions[0].value).show();
    });

    $("form.add-form").submit(function(e){
        var is_form_valid = true;
        $(this).find(".required input, .required textarea").each(function(){
            if (this.value == "") {
                is_form_valid = false;
                $(this.parentElement).addClass("has-error");
            };
        });
        if (!is_form_valid) {
            e.preventDefault();
            $("#necessary-fields").show();
        }
    });

    // Back to top button
    $(window).scroll(function() {
        if ($(this).scrollTop() > MyApp.back_to_top.offset) {
            $('.back-to-top').fadeIn(MyApp.back_to_top.duration);
        } else {
            $('.back-to-top').fadeOut(MyApp.back_to_top.duration);
        }
    });
    
    $('.back-to-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, MyApp.back_to_top.duration);
        return false;
    })

});

function create_table(key, value) {
    var container = $("#content .catalog-table");

    if ($.type(value) != "array") {
        $.each(value, create_table);
    } else {
        
        var table =
        $("<table />", {
            class: "table table-bordered table-striped",
            id: key,
        }).appendTo(container);

        $("<caption />", {
            text: key.replace("_", "/")
        }).appendTo(table);
        
        var thead = $("<thead />", {}).appendTo(table);
        var cur_tr = $("<tr />", {}).appendTo(thead);

        $.each(value[0], function(k, v){
            if (k != 'dev_id') {
                $("<th />", {text: k}).appendTo(cur_tr);
            }
        });
        
        $("<th />", {
            text: '#',
        }).appendTo(cur_tr);

        var tbody = $("<tbody />").appendTo(table);

        value.forEach(function(line){
            cur_tr = $("<tr />", {}).appendTo(tbody);
            
            $.each(line, function(k, v){
                if (k != 'dev_id') {
                    if (v == null) {
                        v = '-';
                    }
                    $("<td />", {text: v}).appendTo(cur_tr);
                }
            });
            $('<td />', {
                html: $('<a />', {
                    class: 'delete-node',
                    href: '#',
                    text: 'delete',
                    rel: line.dev_id,
                    click: delete_node,
                }),
            }).appendTo(cur_tr);
        });
    }

    function delete_node(e) {
        var dev_id = e.target.getAttribute('rel');
        var table = $(e.target).parents('table')[0].getAttribute('id');
        var data = {
            table: table,
            dev_id: dev_id,
        };
        $.ajax({
            url: '/delete/api',
            type: 'POST',
            dataType: 'json',
            data: data,
            params: data,
        }).done(function(resp){
            if (resp.status == 'success') {
                $("#" + this.params.table + " [rel=" + this.params.dev_id + "]").parents('tr').remove();
            } else {
                alert("Can not delete node!");
            }
        });
    }
}

function smooth_show(element) {
    $(element)
    .css({
        opacity: 0,
        display: 'block',
        left: - 200
    })
    .stop()
    .animate({
        opacity: 1,
        left: 0
    }, {
        duration: 1000
    });
}